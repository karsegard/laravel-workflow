<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\Workflow\Workflow;
use KDA\Laravel\Workflow\WorkflowStep;
use KDA\Tests\TestCase;

class ModelTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function instanciate_workflow()
  {
      $wf = Workflow::make();
      $this->assertNotNull($wf);
  }

   /** @test */
   function instanciate_workflow_step()
   {
       $wf = WorkflowStep::make();
       $this->assertNotNull($wf);
   }

  
}