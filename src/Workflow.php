<?php
namespace KDA\Laravel\Workflow;

use Illuminate\Support\Collection;
use KDA\Laravel\Workflow\Contracts\HasWorkflow;

class Workflow
{
    protected Collection $steps;

    protected HasWorkflow $model; 

    public function __construct(){

        $this->steps = collect([]);
        $this->setUp();
    }
    

    public static function make():static
    {
        return new static();
    }

    public function setUp():void
    {
        
    }

    public function addStep($step): static
    {
        $this->steps->push($step);
        return $this;
    }

    public function run(HasWorkflow $model)
    {
        $this->model = $model;

    }
}