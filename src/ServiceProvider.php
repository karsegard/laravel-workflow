<?php
namespace KDA\Laravel\Workflow;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
//use Illuminate\Support\Facades\Blade;
//use KDA\Laravel\Workflow\Facades\Workflow as Facade;
//use KDA\Laravel\Workflow\Workflow as Library;
class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    protected $packageName ='laravel-workflow';
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    public function register()
    {
   /*     parent::register();
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });*/
    }
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
    }
}
